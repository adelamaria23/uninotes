Team name: Fetele de la 1085
Team Topic: Students notes 
Team Members: 
1. Valsan Adela Maria-Project manager 
2.Ursache Ioana-Product Owner 
3.Voinea Sabina-Software developer 
4. Zavoianu Ileana- Design Developer

În cadrul acestei aplicații ne dorim să dezvoltăm un proces prin care să ușurăm munca studenților. 
Aceștia vor dispune de un spațiu în care să își poată încărca notițele proprii referitoare la cursuri, seminare, proiecte și chiar job, care să le fie mereu la îndemână și să îi ajute să se organizeze mai bine.
Aceștia își vor putea organiza notițele calendaristic - pe zile, respectiv săptămâni, într-un spațiu prietenos care va îmbina utilul cu plăcutul.
Pentru a reuși cel din urmă aspect - îmbinarea utilului cu plăcutul, aplicația noastră web va fi și mobile responsive.
De asemenea, vom încerca să facilită experiența studentului printr-un user interface bine realizat.
